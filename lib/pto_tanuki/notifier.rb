# frozen_string_literal: true

module PtoTanuki
  class Notifier
    TEMPLATE = <<~NOTE.strip
      @%<username>s I am out of office from %<start_date>s to %<end_date>s,
      please contact `%<backups>s` for assistance.

      <hr>

      :robot: Auto-responder by [PTO
      Tanuki](https://gitlab.com/pedroms/pto-tanuki). Future mentions of
      my username will not lead to another comment like this one.
    NOTE


    def initialize(gitlab, schedule, backups)
      @gitlab = gitlab
      @schedule = schedule
      @backups = backups
    end

    def notify(todo)
      method =
        if todo.target_type == 'MergeRequest'
          :create_merge_request_note
        elsif todo.target_type == 'Issue'
          :create_issue_note
        end

      return unless method

      @gitlab.public_send(
        method,
        todo.target.project_id,
        todo.target.iid,
        note_body(todo)
      )
    end

    def note_body(todo)
      TEMPLATE % {
        username: todo.author.username,
        start_date: @schedule.start_date.iso8601,
        end_date: @schedule.end_date.iso8601,
        backups: @backups
      }
    end
  end
end
